package Amazing;

public class Cliente {
	private int dni;
	private String nombre;
	private String direccion;
	
	Cliente(int dni, String nombre, String direccion){
		this.dni = dni;
		this.nombre = nombre;
		this.direccion = direccion;
	}

	public int consultarDni() {
		return dni;
	}

	public String consultarDireccion() {
		return direccion;
	}
	
	public String consultarNombre() {
		return nombre;
	}
}
