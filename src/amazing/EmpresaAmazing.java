package Amazing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EmpresaAmazing implements IEmpresa {
	private HashMap<String, Transporte> vehiculos;
	private HashMap<Integer, Pedido> pedidos;
	private String numeroCuit;
	private int numeroPedido = 0;
	private int numeroPaquete = 0;
	private int facturacionTotal = 0;

	EmpresaAmazing(String numeroCuit) {
		this.pedidos = new HashMap<>();
		this.vehiculos = new HashMap<>();
		this.numeroCuit = numeroCuit;
	}

	@Override
	public void registrarAutomovil(String patente, int volMax, int valorViaje, int maxPaq) {
		verificarPatenteDuplicada(patente);
		this.vehiculos.put(patente, new TransporteComun(volMax, valorViaje, maxPaq));
	}

	@Override
	public void registrarUtilitario(String patente, int volMax, int valorViaje, int valorExtra) {
		verificarPatenteDuplicada(patente);
		this.vehiculos.put(patente, new TransporteUtilitario(volMax, valorViaje, valorExtra));
	}

	@Override
	public void registrarCamion(String patente, int volMax, int valorViaje, int adicXPaq) {
		verificarPatenteDuplicada(patente);
		this.vehiculos.put(patente, new TransporteCamion(volMax, valorViaje, adicXPaq));
	}

	@Override
	public int registrarPedido(String cliente, String direccion, int dni) {
		this.pedidos.put(++numeroPedido, new Pedido(dni, cliente, direccion));
		return this.numeroPedido;
	}

	@Override
	public int agregarPaquete(int codPedido, int volumen, int precio, int costoEnvio) {
		Pedido miPedido = buscarPedidoAbierto(codPedido);
		miPedido.agregarPaquete(++this.numeroPaquete, volumen, precio, costoEnvio);
		return this.numeroPaquete;
	}

	@Override
	public int agregarPaquete(int codPedido, int volumen, int precio, int porcentaje, int adicional) {
		Pedido miPedido = buscarPedidoAbierto(codPedido);
		miPedido.agregarPaquete(++this.numeroPaquete, volumen, precio, adicional, porcentaje);
		return this.numeroPaquete;
	}

	@Override
	public boolean quitarPaquete(int codPaquete) {
		if (codPaquete < 1 || codPaquete > this.numeroPaquete)
			throw new RuntimeException();

		for (Pedido ped : this.pedidos.values()) {
			if (ped.eliminarPaquete(codPaquete))
				return true;
		}
		return false;
	}

	@Override
	public double cerrarPedido(int codPedido) {
		Pedido miPedido = buscarPedidoAbierto(codPedido);
		this.facturacionTotal += miPedido.consultarTotal();
		return miPedido.cerrarPedido();
	}

	@Override
	public String cargarTransporte(String patente) {
		Transporte miVehiculo = buscarTransporte(patente);
		StringBuilder listaPaquetesCargados = new StringBuilder();
		int numPedidoActual = 1;

		for (Pedido ped : this.pedidos.values()) {
			if (ped.estaCerrado()) {
				ArrayList<Paquete> paquetes = ped.devolverPaquetesSinEntregar();
				for (Paquete paq : paquetes) {
					if (miVehiculo.cargarPaquete(paq)) {
						listaPaquetesCargados.append(
								" + [ " + numPedidoActual + " - " + paq.consultarCodigo() + " ] " + ped.consultarDireccion());
						listaPaquetesCargados.append("\n");
					}
				}
			}
			numPedidoActual++;
		}
		return listaPaquetesCargados.toString();
	}

	@Override
	public double costoEntrega(String patente) {
		Transporte miVehiculo = buscarTransporte(patente);
		return miVehiculo.calcularCostoViaje();
	}

	@Override
	public Map<Integer, String> pedidosNoEntregados() {
		Map<Integer, String> pedidosNoEntregadosAux = new HashMap<>();

		int codigoPedido = 1;
		for (Pedido ped : this.pedidos.values()) {
			if (ped.estaCerrado() && !ped.estaTerminado()) {
				pedidosNoEntregadosAux.put(codigoPedido, ped.consultarCliente());
			}
			codigoPedido++;
		}
		return pedidosNoEntregadosAux;
	}

	@Override
	public double facturacionTotalPedidosCerrados() {
		return this.facturacionTotal;
	}

	@Override
	public boolean hayTransportesIdenticos() {
		boolean transportesIdenticos = false;

		for (String patente1 : vehiculos.keySet()) {
			Transporte transporte1 = vehiculos.get(patente1);
			for (String patente2 : vehiculos.keySet()) {
				if (!patente1.equals(patente2)) {
					Transporte transporte2 = vehiculos.get(patente2);
					transportesIdenticos |= transporte1.sonIdenticos(transporte2);
				}
			}
		}
		return transportesIdenticos;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Empresa Amazing - CUIT : [ " + this.numeroCuit + " ]");
		sb.append("\nPedidos de la empresa:\n");
		
		for(Integer codPedido : pedidos.keySet()) {
			sb.append("ID Pedido : " + codPedido + " ");
			sb.append("\n");
			sb.append(pedidos.get(codPedido).toString());
			sb.append("\n");
		}
		
		sb.append("\nVehiculos de la empresa:\n");
		for(String patente: vehiculos.keySet()) {
			sb.append("Patente : " + patente + " ");
			sb.append("\n");
			sb.append(vehiculos.get(patente).toString());
			sb.append("\n");
		}
		return sb.toString();
	}

	// ----------METODOS AUXILIARES---------------
	private Pedido buscarPedidoAbierto(int numPedido) {
		Pedido pedido = this.pedidos.get(numPedido);
		if (pedido == null)
			throw new RuntimeException("El pedido no existe");
		if (pedido.estaCerrado())
			throw new RuntimeException("El pedido esta cerrado");

		return pedido;
	}

	private Transporte buscarTransporte(String patente) {
		Transporte aux = vehiculos.get(patente);
		if (aux == null)
			throw new RuntimeException("No se encontro un vehiculo con esa patente");
		return aux;
	}

	private void verificarPatenteDuplicada(String patente) {
		if (vehiculos.containsKey(patente))
			throw new RuntimeException();
	}
}
