package Amazing;
public abstract class Paquete {
	protected int id;
	protected int volumen;
	protected int precio;
	protected int costoExtra;
	protected boolean fueEntregado = false;
	
	Paquete(int id, int volumen, int precio, int costoExtra){
		this.id = id;
		this.precio = precio;
		this.volumen = volumen;
		this.costoExtra = costoExtra;
	}
	
	public boolean fueEntregado() {
		return this.fueEntregado;
	}
	
	public void actualizarEstado(){
		this.fueEntregado = true;
	}
	
	public int consultarVolumen() {
		return this.volumen;
	}
	
	public int consultarCodigo() {
		return this.id;
	}
	
	public boolean mismoCodigo(int numeroPaquete) {
		return id == numeroPaquete;
	}
	
	public boolean tieneMenosVolumenQue(int volumen) {
		return this.volumen < volumen;
	}
	
	public boolean tieneMasVolumenQue(int volumen) {
		return this.volumen > volumen;
	}
	
	@Override
	public String toString() {
		return "ID = " + id + ", Volumen: " + volumen + ", Precio: " + precio + ", Costo Extra: " + costoExtra; 
	}
	
	//METODOS ABSTRACTOS------------------------
	
	abstract public int consultarPrecio();
	
	abstract public boolean equals(Object other);
}
