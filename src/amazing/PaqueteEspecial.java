package Amazing;

public class PaqueteEspecial extends Paquete {
	private int porcentaje;

	PaqueteEspecial(int id, int volumen, int precio, int costoExtra, int porcentaje) {
		super(id, volumen, precio, costoExtra);
		this.porcentaje = porcentaje;
	}

	@Override
	public int consultarPrecio() {
		int precioBaseConPorcentaje = precio + ( (precio * porcentaje) / 100);

		if (this.volumen >= 3000) {
			precioBaseConPorcentaje += this.costoExtra;
			if (this.volumen >= 5000)
				precioBaseConPorcentaje += this.costoExtra;
		}

		return precioBaseConPorcentaje;
	}

	@Override
	public String toString() {
		return super.toString() + ", porcentaje extra: " + porcentaje;
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof PaqueteEspecial))
			return false;

		PaqueteEspecial otherAux = (PaqueteEspecial) other;

		if (this.precio != otherAux.precio)
			return false;

		if (this.volumen != otherAux.volumen)
			return false;

		if (this.costoExtra != otherAux.costoExtra)
			return false;
		
		if (this.porcentaje != otherAux.porcentaje)
			return false;

		return true;
	}
}