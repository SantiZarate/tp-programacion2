package Amazing;

public class PaqueteOrdinario extends Paquete {

	PaqueteOrdinario(int id, int volumen, int precio, int costoExtra) {
		super(id, volumen, precio, costoExtra);
	}

	@Override
	public int consultarPrecio() {
		return super.precio + super.costoExtra;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof PaqueteOrdinario))
			return false;

		PaqueteOrdinario otherAux = (PaqueteOrdinario) other;

		if (this.precio != otherAux.precio)
			return false;

		if (this.volumen != otherAux.volumen)
			return false;

		if (this.costoExtra != otherAux.costoExtra)
			return false;

		return true;
	}
}
