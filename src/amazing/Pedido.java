package Amazing;

import java.util.ArrayList;
import java.util.Iterator;

public class Pedido {
	private Cliente cliente;
	private ArrayList<Paquete> carritoDeCompras;
	private double total = 0;
	private boolean estaCerrado = false;

	public Pedido(int dni, String nombreCliente, String direccion) {
		this.carritoDeCompras = new ArrayList<>();
		this.cliente = new Cliente(dni, nombreCliente, direccion);
	}

	public void agregarPaquete(int numeroPaquete, int volumen, int precio, int adicional) {
		PaqueteOrdinario nuevoPaquete = new PaqueteOrdinario(numeroPaquete, volumen, precio, adicional);
		this.total += nuevoPaquete.consultarPrecio();
		System.out.println("Paquete agregado, " + nuevoPaquete);
		this.carritoDeCompras.add(nuevoPaquete);
	}
	
	public void agregarPaquete(int numeroPaquete, int volumen, int precio, int adicional, int porcentaje) {
		PaqueteEspecial nuevoPaquete = new PaqueteEspecial(numeroPaquete, volumen, precio, adicional, porcentaje);
		this.total += nuevoPaquete.consultarPrecio();
		System.out.println("Paquete agregado, " + nuevoPaquete);
		this.carritoDeCompras.add(nuevoPaquete);
	}

	public boolean eliminarPaquete(int codPaquete) {
		Iterator<Paquete> paquetesIterador = this.carritoDeCompras.iterator();

		while (paquetesIterador.hasNext()) {
			Paquete it = paquetesIterador.next();
			if (it.mismoCodigo(codPaquete)) {
				paquetesIterador.remove();
				return true;
			}
		}
		return false;
	}
	
	public boolean estaTerminado() {
		boolean todosLosPaquetesEntregados = true;
		
		for(Paquete paq : this.carritoDeCompras) 
			todosLosPaquetesEntregados &= paq.fueEntregado;
		
		return todosLosPaquetesEntregados;
	}
	
	public ArrayList<Paquete> devolverPaquetesSinEntregar(){
		ArrayList<Paquete> aux = new ArrayList<>();
		
		for(Paquete paq : this.carritoDeCompras) {
			if (!paq.fueEntregado)
				aux.add(paq);
		}
		return aux;
	}
	
	public double consultarTotal() {
		return this.total;
	}
	
	public boolean estaCerrado() {
		return this.estaCerrado;
	}
	
	public String consultarCliente() {
		return cliente.consultarNombre();
	}
	
	public String consultarDireccion() {
		return this.cliente.consultarDireccion();
	}
	
	public double cerrarPedido() {
		this.estaCerrado = true;
		return consultarTotal();
	}
	
	@Override
	public String toString() {
		if(carritoDeCompras.isEmpty()) return "Pedido vacio...\n";
		
		StringBuilder sb = new StringBuilder();
		for(Paquete paq : carritoDeCompras) {
			sb.append("[ ");
			sb.append(paq.toString());
			sb.append(" ]");
			sb.append("\n");
		}
		return sb.toString();
	}
}