package Amazing;

import java.util.ArrayList;

public abstract class Transporte {
	protected int volumenMax;
	protected int volumenOcupado = 0;
	protected double precioViaje;
	protected ArrayList<Paquete> paquetes;

	Transporte(int volumenMax, double precioViaje) {
		this.volumenMax = volumenMax;
		this.precioViaje = precioViaje;
		this.paquetes = new ArrayList<>();
	}

	public double calcularCostoViaje() {
		if (paquetes.isEmpty())
			throw new RuntimeException();
		return precioViaje;
	}

	public boolean estaVacio() {
		return this.paquetes.isEmpty();
	}

	protected boolean hayVolumenSuficiente(Paquete paquete) {
		return (volumenMax - volumenOcupado) > paquete.consultarVolumen();
	}

	public boolean sonIdenticos(Transporte transporte) {
		return sonDelMismoTipo(transporte) && tienenCargaIdentica(transporte)
				&& !this.estaVacio();
	}
	
	@Override
	public String toString() {
		return "volumenMax: " + volumenMax + ", Precio Por Viaje: " + precioViaje;
	}
	
	public boolean cargarPaquete(Paquete paquete) {
		if (!paqueteCumpleLasCondiciones(paquete)) return false;

		this.paquetes.add(paquete);
		this.volumenOcupado += paquete.consultarVolumen();
		paquete.actualizarEstado();
		return true;
	}
	
	// METODOS PRIVADOS------------------------------
	
	private boolean sonDelMismoTipo(Transporte transporte) {
		return (this.getClass() == transporte.getClass());
	}

	private boolean tienenCargaIdentica(Transporte transporte) {
		if (this.paquetes.size() != transporte.paquetes.size())
			return false;
		
		boolean paquetesIguales = true;
		
		for(Paquete paq : this.paquetes) {
			 paquetesIguales &= hayUnPaqueteIdentico(paq, transporte);
		}
		
		return paquetesIguales;
	}
	
	private boolean hayUnPaqueteIdentico(Paquete paquete, Transporte transporte) {
		boolean aux = false;

		for(Paquete paq : transporte.paquetes)
			aux |= paquete.equals(paq);
		
		return aux;
	}
	
	// METODOS ABSTRACTOS------------------------------
	
	protected abstract boolean paqueteCumpleLasCondiciones(Paquete paquete);
}
