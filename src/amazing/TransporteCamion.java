package Amazing;

public class TransporteCamion extends Transporte {
	private int valorExtraPorPaquete;

	public TransporteCamion(int volumenMax, double precioViaje, int valorExtraPorPaquete) {
		super(volumenMax, precioViaje);
		this.valorExtraPorPaquete = valorExtraPorPaquete;
	}

	@Override
	public double calcularCostoViaje() {
		double costoTotal = super.calcularCostoViaje();
		double adicionalTotal = this.valorExtraPorPaquete * super.paquetes.size();
		return costoTotal + adicionalTotal;
	}

	@Override
	public String toString() {
		return super.toString() + ", Adicional Por Paquete: " + valorExtraPorPaquete;
	}

	// Metodos privados ---------------------------------------

	protected boolean paqueteCumpleLasCondiciones(Paquete paquete) {
		boolean esDeTipoEspecial = paquete instanceof PaqueteEspecial;
		boolean volumenPaquete = paquete.tieneMasVolumenQue(2000);
		return esDeTipoEspecial && volumenPaquete && super.hayVolumenSuficiente(paquete);
	}

}
