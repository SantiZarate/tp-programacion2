package Amazing;

public class TransporteComun extends Transporte {
	private int maxPaq;

	public TransporteComun(int volumenMax, double precioViaje, int maxPaq) {
		super(volumenMax, precioViaje);
		this.maxPaq = maxPaq;
	}

	protected boolean paqueteCumpleLasCondiciones(Paquete paquete) {
		boolean esDeTipoOrdinario = paquete instanceof PaqueteOrdinario;
		boolean volumenPaquete = paquete.tieneMenosVolumenQue(2000);
		boolean limiteDePaquetesAlcanzado = (maxPaq - super.paquetes.size()) != 0;

		return esDeTipoOrdinario && volumenPaquete && limiteDePaquetesAlcanzado && super.hayVolumenSuficiente(paquete);
	}
	
	@Override
	public String toString() {
		return super.toString() + ", cantidad Maxima de Paquetes: " + maxPaq;
	}
}
