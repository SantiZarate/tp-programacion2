package Amazing;

public class TransporteUtilitario extends Transporte {
	private int valorExtra;

	public TransporteUtilitario(int volumenMax, double precioViaje, int valorExtra) {
		super(volumenMax, precioViaje);
		this.valorExtra = valorExtra;
	}

	@Override
	public double calcularCostoViaje(){
		double costoViaje = super.calcularCostoViaje();
		if (super.paquetes.size() > 3)
			costoViaje += valorExtra;

		return costoViaje;
	}

	protected boolean paqueteCumpleLasCondiciones(Paquete paquete) {
		boolean esOrdinario = paquete instanceof PaqueteOrdinario;
		boolean esEspecial = paquete instanceof PaqueteEspecial;

		return (esOrdinario || esEspecial) && super.hayVolumenSuficiente(paquete);
	}
	
	@Override
	public String toString() {
		return super.toString() + ", valor Extra: " + valorExtra;
	}
}
