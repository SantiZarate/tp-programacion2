package Amazing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class UnitTest {
	private EmpresaAmazing amazing;
	
	@Before
	public void startUp() throws Exception {
		amazing = new EmpresaAmazing("11-23214214-05");
		amazing.registrarPedido("Santiago Zarate", "Av. Olivos", 43869445);

		//Agregando paquetes
		amazing.agregarPaquete(1, 500, 3000, 200);
		amazing.agregarPaquete(1, 500, 7000, 200);
		amazing.agregarPaquete(1, 50, 5000, 200);
		
		amazing.registrarPedido("Mario Santo", "Av. Olivos", 43869443);
		amazing.agregarPaquete(2, 50, 5000, 200);
		amazing.agregarPaquete(2, 50, 5000, 200);
		
		amazing.cerrarPedido(2);
	}
	
	@Test
	public void registrarPedidoDistintos() {
		int numeroPedido = amazing.registrarPedido("martin leiva", "av. olivos", 73826223);
		assertEquals(3, numeroPedido);
	}
	
	@Test
	public void eliminarPaqueteEsTrue() throws Exception {
		assertTrue(amazing.quitarPaquete(3));
	}
	
	@Test
	public void cerrarPedidoDevuelveMismoValor() throws Exception {
		assertTrue(amazing.cerrarPedido(1) == 15600);
	}
}
